# -*- coding: utf-8 -*-
"""
Created on Thu Oct  6 13:05:23 2022

@author: HP
"""
# Basic libraries to import for completing the whole work.

import sys
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets
import csv
import pandas as pd

class Mainwindow(QMainWindow):
    def __init__(self):
        super(Mainwindow, self).__init__()
        loadUi("mainWindow.ui",self)
        

# main

app = QApplication(sys.argv)
welcome = Mainwindow()
welcome.show()

try:
    sys.exit(app.exec())
except:
    print("Exiting")
